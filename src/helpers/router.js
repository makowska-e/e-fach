import Vue from 'vue';
import Router from 'vue-router';

import HomePage from '../components/home/HomePage'
import LoginPage from '../components/login/LoginPage'
import RegisterPage from '../components/register/RegisterPage'
import PersonalProfilePage from '../components/profiles/PersonalProfile'
import MapComponent from '../components/map/MapComponent'
import CompanyProfileComponent from '../components/company/CompanyProfileComponent'
import ProfessionalProfileComponent from '../components/professional/ProfessionalProfileComponent'
import SearchResultComponent from '../components/searchResult/SearchResultComponent'

Vue.use(Router);

export const router = new Router({
    mode: 'history',
    routes: [
        {path: '/', component: HomePage},
        {path: '/login', component: LoginPage},
        {path: '/register', component: RegisterPage},
        {path: '/personalProfile', component: PersonalProfilePage},
        {path: '/map', component: MapComponent},
        {path: '/companyProfile', component: CompanyProfileComponent},
        {path: '/professionalProfile', component: ProfessionalProfileComponent},
        {path: '/searchresult', name: 'searchresult', component: SearchResultComponent, props: true},

        // otherwise redirect to home
        {path: '*', redirect: '/'}
    ]
});

router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const publicPages = ['/login', '/register', '/', '/personalProfile', '/map', '/companyProfile', '/professionalProfile', '/searchresult'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('user');

    if (authRequired && !loggedIn) {
        return next('/');
    }

    next();
})