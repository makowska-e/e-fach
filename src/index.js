import Vue from 'vue';
import VeeValidate from 'vee-validate';
import Vuetify from 'vuetify';
import DaySpanVuetify from 'dayspan-vuetify'

import { store } from './store';
import { router } from './helpers';
import App from './app/App';
import BootstrapVue from 'bootstrap-vue';
import * as VueGoogleMaps from "vue2-google-maps";

import { Layout } from 'bootstrap-vue/es/components';

import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'dayspan-vuetify/dist/lib/dayspan-vuetify.min.css'

Vue.use(VeeValidate, BootstrapVue);

Vue.use(Vuetify);

Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyCTr-88IUVCik54LhE-Rv8urHbAKHR3aac",
        libraries: "places" // necessary for places input
    }
});

Vue.use(DaySpanVuetify, {
    methods: {
        getDefaultEventColor: () => '#1976d2'
    }
});

Vue.use(Layout);

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});